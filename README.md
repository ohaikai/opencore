# OpenCore 0.6.7 Big Sur 11.3 Gigabyte AORUS z490i / NZXT H1 / RX580

Hello friends,

This repo holds my build for a fully working OpenCore install. I'll add some more details / credits later.

If you do pull this repo, please make sure to edit the config.plist with your own serials, the current ones are placeholders.

This machine is for educational purposes only. It shouldn't be used for profit in any way.

# Bluetooth Airpod / Misc. Bluetooth Headset Microphone issues FIX

One of the things that initially made me rethink my purchase of a BT5 motherboard was the fact that whenever I tried to use my Airpods for a phone call / Zoom / Teams meeting it would cause the Airpods to malfunction and refuse to play any audio whatsoever until a full disconnect and reconnect. This was with using the IntelBluetoothFirmware.kext.

After much sleuthing of various Hackintosh boards, I believe the reason might be because the Bluetooth firmware provided doesn't like how MacOS tries to swap to a lower quality codec as soon as the mic is engaged. I've posted my findings on the Issues page on GitHub, but since there's no real documentation as to how to contribute to that project, I present to you my fix:

Windows Dual Boot

If you setup and boot in Windows, you are then able to install the newest firmware / drivers provided by Intel for the AX201 set. 

https://downloadcenter.intel.com/download/30251/Intel-Wireless-Bluetooth-for-Windows-10

Once those are properly loaded, you must restart the computer from the OS (Not via the power button) and then select Mac from the Boot (OpenCanopy) menu. This will allow the most recent firmware to persist until the new hard power cycle, and magically your Bluetooth Mic will work without issues.

Feel free to thumbs up the issue here:

https://github.com/OpenIntelWireless/IntelBluetoothFirmware/issues/262

# WIFI Fix?

Unfortunately I could not get the WIFI6 to go anywhere past 100mb/s regardless of the amount of tuning. This is also a somewhat known limitation by the creators of the itlwm driver, it seems they are still working to get both higher bands as well as dual-band working.

# Themes
https://github.com/LuckyCrack/OpenCore-Themes
